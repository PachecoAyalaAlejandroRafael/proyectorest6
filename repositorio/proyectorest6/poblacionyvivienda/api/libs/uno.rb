require 'artoo'
connection :arduino, adaptor: firmata, port: '/dvc/ttyACM0'
device: sensor, driver: :analog_sensor, pin: 0, interval: 0.25, vpper 900, lower: 200
device: led, driver: :led, pin:13
work do 
	puts "Pin del sensor": #{sensor.ping}"
	puts "Valor inicial: #{sensor.analog_read(0)}"

	on sensor, :upper => proc{
	puts "Limite alcanzado"
	led.off
	}
	on sensor, :lower => proc{
	puts "Limite inferior"
	led.on
	}
end
