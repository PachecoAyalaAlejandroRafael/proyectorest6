function crear_form(nombre,id)
{
  var obj=document.createElement("form");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_tabla(titulo,dato)
{
  var tabla=document.createElement("table");
  var cabecera=document.createElement("thead");
  var titulos=document.createElement("tr");
  for(var i=0;i<titulo.length;i++)
  {
    var t1=titulo[i];
    var elemento=document.createElement("th");
    var contenido=document.createTextNode(t1);
    elemento.appendChild(contenido);
    titulos.appendChild(elemento);
  }
  cabecera.appendChild(titulos);
  tabla.appendChild(cabecera);
  var cuerpo=document.createElement("tbody");
  for(var i=0;i<dato.length;i++)
  {
    var datos=document.createElement("tr");
    for(var j=0;j<dato[i].length;j++)
    {
      var dato1=dato[i][j];
      var elemento1=document.createElement("td");
      var contenido1=document.createTextNode(dato1);
      elemento1.appendChild(contenido1);
      datos.appendChild(elemento1);
    }
    cuerpo.appendChild(datos);
  }
  tabla.appendChild(cuerpo);
  return tabla;
}


function crear_select(nombre,id,opcion,cambio)
{
  var obj=document.createElement("select");
  obj.setAttribute("type","select");
  obj.setAttribute("name",nombre);
  obj.setAttribute("onChange",cambio);
  obj.setAttribute("id",id);
  for(var i=0;i<opcion.length;i++)
  {
    var op=document.createElement("option");
    op_text=document.createTextNode(opcion[i][1]);
    op.appendChild(op_text);
    op.setAttribute("value",opcion[i][0]);
    obj.appendChild(op);
  }
  return obj;
}

function crear_container(cadena)
{
  var obj=document.createElement("div");
  obj.setAttribute("class","container-fluid");
  obj.setAttribute("id",cadena);
  var ob=document.createElement("legend");
  var ob_text=document.createTextNode(cadena);
  ob.appendChild(ob_text);
  obj.appendChild(ob);
  return obj;
}

function asociar_hijo(hijo,padre)
{
  padre.appendChild(hijo);
}

function div(cadena,objeto,id)
{
  var obj=document.createElement("div");
  obj.setAttribute("id",id);
  obj.setAttribute("class","row");
  var ob=document.createElement("div");
  ob.setAttribute("class","col-md-2");
  var titulo=document.createElement("h1");
  var ob_text=document.createTextNode(cadena);
  ob.appendChild(ob_text);
  var ob1=document.createElement("div");
  ob1.setAttribute("class","col-md-10");
  ob1.appendChild(objeto);
  obj.appendChild(ob);
  obj.appendChild(ob1);
  return obj;
}

function iniciar()
{
  var url="http://js.wapp.bitnami/poblacionyvivienda/temas/";
    var objeto_xhr=new XMLHttpRequest();
  objeto_xhr.onreadystatechange =function(){
  if(this.readyState===4&&this.status===200)
  {
    console.log("exito");
    console.log(objeto_xhr.responseXML); 
    var objeto=objeto_xhr.responseXML.firstChild.children;

titulo=[];

for(var i=0;i<objeto.length;i++)
{
  var id=objeto[i].children[0].textContent;
  var des=objeto[i].children[1].textContent;
  titulo[i]=[id,des];
}
  
  var cuerpo=document.body;
  var contenedor=crear_container("CENSO POBLACION Y VIVIENDA 2010");
  var formulario=crear_form("formulario","101");
  var selec=crear_select("opciones","102",titulo,"valor()");
  var elemento=div("TEMAS NIVEL 1",selec);

  asociar_hijo(formulario,cuerpo);
  asociar_hijo(contenedor,formulario);
  asociar_hijo(elemento,contenedor);
  }}
  objeto_xhr.open('GET',url);
  objeto_xhr.setRequestHeader('accept','application/xml');  
  objeto_xhr.send(); 
  
  
}

function valor()
{
 
var op=document.formulario.opciones.value;

var url="http://js.wapp.bitnami/poblacionyvivienda/temas/"+op+"/subtemas";
var objeto_xhr=new XMLHttpRequest();
  objeto_xhr.onreadystatechange =function(){
  if(this.readyState===4&&this.status===200)
  {
    console.log("exito");
    console.log(objeto_xhr.responseXML); 
    var objeto=objeto_xhr.responseXML.firstChild.children;

titulo=[];

for(var i=0;i<objeto.length;i++)
{
  var id=objeto[i].children[0].children[0].textContent;
  var des=objeto[i].children[0].children[1].textContent;
  titulo[i]=[id,des];
}
  
var selec=crear_select("opciones2","103",titulo,"valor2()");
var estilo4=div("TEMAS NIVEL 2",selec);

    var contenedor=document.getElementById("CENSO POBLACION Y VIVIENDA 2010");

asociar_hijo(estilo4,contenedor);
  }}
  objeto_xhr.open('GET',url);
  objeto_xhr.setRequestHeader('accept','application/xml');  
  objeto_xhr.send(); 
  
  
}


function valor2()
{
var op=document.formulario.opciones.value;
var op2=document.formulario.opciones2.value;
  
var url="http://js.wapp.bitnami/poblacionyvivienda/temas/"+op+"/subtemas"+"/"+op2+"/subtemas";
var objeto_xhr=new XMLHttpRequest();
  objeto_xhr.onreadystatechange =function(){
  if(this.readyState===4&&this.status===200)
  {
    console.log("exito");
    console.log(objeto_xhr.responseXML); 
    var objeto=objeto_xhr.responseXML.firstChild.children;

titulo=[];

for(var i=0;i<objeto.length;i++)
{
  var id=objeto[i].children[0].children[0].children[0].textContent;
  var des=objeto[i].children[0].children[0].children[1].textContent;
  titulo[i]=[id,des];
}
  
var selec=crear_select("opciones3","104",titulo,"valor3()");
var estilo4=div("TEMAS NIVEL 3",selec);
    
        var contenedor=document.getElementById("CENSO POBLACION Y VIVIENDA 2010");


asociar_hijo(estilo4,contenedor);
    valor3();
  }}
  objeto_xhr.open('GET',url);
  objeto_xhr.setRequestHeader('accept','application/xml');  
  objeto_xhr.send(); 
  
  
}

function valor3(op,op2,OP3)
{
var op=document.formulario.opciones.value;
var op2=document.formulario.opciones2.value;
var op3=document.formulario.opciones3.value;

  var url="http://js.wapp.bitnami/poblacionyvivienda/temas/"+op+"/subtemas"+"/"+op2+"/subtemas"+"/"+op3+"/indicadores";
var objeto_xhr=new XMLHttpRequest();
  objeto_xhr.onreadystatechange =function(){
  if(this.readyState===4&&this.status===200)
  {
    console.log("exito");
    console.log(objeto_xhr.responseXML); 
    var objeto=objeto_xhr.responseXML.firstChild.children;

dato=[];
for(var i=0;i<objeto.length;i++)
{
  var id=i+1;
  var des=objeto[i].children[0].children[0].children[0].children[1].textContent;
  var con=objeto[i].children[0].children[0].children[0].children[2].textContent;

  dato[i]=[id,des,con];
}

titulo=[
  "Caracteristica","Descripcion","censo 2010"
  ];
  
  
 var contenedor2=crear_container("INDICADORES");
asociar_hijo(contenedor2,formulario);
  
var tabla=crear_tabla(titulo,dato);
var estilo4=div("TABLA",tabla);

asociar_hijo(estilo4,contenedor2);
    
    var boton=crear_button("boton","201","CONTINUAR");
    var estilo5=div("DE CLIC EN EL BOTON CONTINUAR",boton);

asociar_hijo(estilo5,contenedor2);
    boton=document.getElementById("201");
boton.addEventListener('click',iniciar,false);
  }}
  objeto_xhr.open('GET',url);
  objeto_xhr.setRequestHeader('accept','application/xml');  
  objeto_xhr.send(); 
  
  
}
function crear_button(nombre,id,cadena)
{
  var obj=document.createElement("button");
  obj_text=document.createTextNode(cadena);
  obj.appendChild(obj_text);
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  obj.setAttribute("type","button");
  return obj;
}

iniciar();







